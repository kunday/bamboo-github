FROM ubuntu:14.04

RUN apt-get update
RUN apt-get install -y python-software-properties software-properties-common
RUN apt-add-repository -y ppa:brightbox/ruby-ng

RUN apt-get update
RUN apt-get install -y ruby2.1 git-core && gem install bundler

ADD . bamboo-github
WORKDIR bamboo-github
RUN bundle package --all

CMD []
