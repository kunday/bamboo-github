# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'bamboo/github/version'

Gem::Specification.new do |spec|
  spec.name          = "bamboo-github"
  spec.version       = Bamboo::Github::VERSION
  spec.authors       = ["Arvind Kunday"]
  spec.email         = ["hi@kunday.com"]
  spec.summary       = %q{Report bamboo build status to github.}
  spec.description   = %q{Mark commits with their build status.}
  spec.homepage      = "http://kunday.com"
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0")
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.7"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_runtime_dependency "octokit"
end
